#!/bin/bash
# ------------------------------------------------------------------------------
# Script name:  bhc_hipp_masking.sh
#
# Description:  Script to mask out the CSF from the FIRST hippocampal masks
#
# Author:       Grace Gillis, Ludo Griffanti, 2021
#
# ------------------------------------------------------------------------------

[ _$1 = _ ] && exit

. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header 

origDir=`pwd`

cd $1/T1

#isolate L Hipp
${FSLDIR}/bin/fslmaths T1_first/T1_first_all_fast_firstseg.nii.gz -thr 16.5 -uthr 17.5 -bin T1_first/T1_first_all_fast_firstseg_L_hipp.nii.gz
#isolate R Hipp
${FSLDIR}/bin/fslmaths T1_first/T1_first_all_fast_firstseg.nii.gz -thr 52.5 -uthr 53.5 -bin T1_first/T1_first_all_fast_firstseg_R_hipp.nii.gz

#threshold, binarise, and invert pve_0 map to create mask
${FSLDIR}/bin/fslmaths T1_fast/T1_brain_pve_0.nii.gz -thr 0.7 -binv T1_fast/T1_brain_pve_0_thr0_7.nii.gz

#apply masks 
${FSLDIR}/bin/fslmaths T1_first/T1_first_all_fast_firstseg_L_hipp.nii.gz -mul T1_fast/T1_brain_pve_0_thr0_7.nii.gz T1_first/T1_first_all_fast_firstseg_L_hipp_CSFmasked_0_7.nii.gz
${FSLDIR}/bin/fslmaths T1_first/T1_first_all_fast_firstseg_R_hipp.nii.gz -mul T1_fast/T1_brain_pve_0_thr0_7.nii.gz T1_first/T1_first_all_fast_firstseg_R_hipp_CSFmasked_0_7.nii.gz

#Setting the string of NaN in case there is a problem.
numVars="4"
result="";
for i in $(seq 1 $numVars) ; do 
    result="NaN $result" ; 
done 
# Deriving volumes
L_hipp=`${FSLDIR}/bin/fslstats T1_first/T1_first_all_fast_firstseg_L_hipp.nii.gz -V | awk '{print $2}'`
R_hipp=`${FSLDIR}/bin/fslstats T1_first/T1_first_all_fast_firstseg_R_hipp.nii.gz -V | awk '{print $2}'`
L_hipp_masked=`${FSLDIR}/bin/fslstats T1_first/T1_first_all_fast_firstseg_L_hipp_CSFmasked_0_7.nii.gz -V | awk '{print $2}'`
R_hipp_masked=`${FSLDIR}/bin/fslstats T1_first/T1_first_all_fast_firstseg_R_hipp_CSFmasked_0_7.nii.gz -V | awk '{print $2}'`
result="$L_hipp $R_hipp $L_hipp_masked $R_hipp_masked" ;

# copying in IDP folder (creating it if does not exist)
if [ ! -d ../IDP_files ]; then
    mkdir -p ../IDP_files
fi

echo $result > ../IDP_files/bb_IDP_FIRST_hippo_CSFmasked.txt

cd $origDir