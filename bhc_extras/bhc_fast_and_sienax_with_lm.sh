#!/bin/sh
# ------------------------------------------------------------------------------
# Script name: fast_and_sienax_with_lm
#
# Description: Script to run a reduced version of sienax adapted with lesion-masking.
# ------------------------------------------------------------------------------
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith & Mark Jenkinson; adapted by Grace Gillis


[ _$1 = _ ] && exit

# input is subject's directory, assuming data organised according to UKB pipeline v1.5
origDir=`pwd`
basedir=${1}
cd ${basedir}/T1

mkdir T1_fast_sienax_lm
cd T1_fast_sienax_lm

${FSLDIR}/bin/bet ../T1 T1_brain -s 
${FSLDIR}/bin/imrm T1_brain

${FSLDIR}/bin/pairreg ${FSLDIR}/data/standard/MNI152_T1_2mm_brain ../T1_brain ${FSLDIR}/data/standard/MNI152_T1_2mm_skull T1_brain_skull T1_to_MNI_linear.mat >> report.sienax 2>&1

${FSLDIR}/bin/avscale T1_to_MNI_linear.mat ${FSLDIR}/data/standard/MNI152_T1_2mm > T1_to_MNI_linear.avscale
xscale=`grep Scales T1_to_MNI_linear.avscale | awk '{print $4}'`
yscale=`grep Scales T1_to_MNI_linear.avscale | awk '{print $5}'`
zscale=`grep Scales T1_to_MNI_linear.avscale | awk '{print $6}'`
vscale=`echo "10 k $xscale $yscale * $zscale * p"|dc -`
echo "VSCALING $vscale" >> report.sienax

${FSLDIR}/bin/flirt -in ../T1          -ref ${FSLDIR}/data/standard/MNI152_T1_1mm -o T1_to_MNI_linear             -applyxfm -init T1_to_MNI_linear.mat -interp spline
${FSLDIR}/bin/flirt -in T1_brain_skull -ref ${FSLDIR}/data/standard/MNI152_T1_1mm -o T1_brain_skull_to_MNI_linear -applyxfm -init T1_to_MNI_linear.mat -interp trilinear

${FSLDIR}/bin/applywarp --rel --interp=trilinear --in=${FSLDIR}/data/standard/MNI152_T1_2mm_strucseg_periph --ref=../T1 -w ../transforms/T1_to_MNI_warp_coef_inv -o T1_segperiph
${FSLDIR}/bin/fslmaths T1_segperiph -thr 0.5 -bin T1_segperiph

${FSLDIR}/bin/fslmaths ${FSLDIR}/data/standard/MNI152_T1_2mm_strucseg -thr 4.5 -bin T1_segvent
${FSLDIR}/bin/applywarp --rel --interp=nn --in=T1_segvent --ref=../T1 -w ../transforms/T1_to_MNI_warp_coef_inv -o T1_segvent

#run FAST and lesion-masking
lm=../../T2_FLAIR/lesions/final_mask.nii.gz
${FSLDIR}/bin/fslmaths $lm -bin -mul -1 -add 1 -mul ../T1_brain T1_brain_mask -odt float
${FSLDIR}/bin/fast -g T1_brain_mask >> report.sienax 2>&1
${FSLDIR}/bin/fslmaths $lm -bin -max T1_brain_mask_pve_2 T1_brain_mask_pve_2 -odt float
${FSLDIR}/bin/fslmaths $lm -bin -mul 3 -max T1_brain_mask_seg T1_brain_mask_seg -odt int

echo "tissue             volume    unnormalised-volume" >> report.sienax

${FSLDIR}/bin/fslmaths T1_brain_mask_pve_1 -mas T1_segperiph T1_pve_1_segperiph -odt float
S=`${FSLDIR}/bin/fslstats T1_pve_1_segperiph -m -v`
xa=`echo $S | awk '{print $1}'`
xb=`echo $S | awk '{print $3}'`
uxg=`echo "2 k $xa $xb * 1 / p" | dc -`
xg=`echo "2 k $xa $xb * $vscale * 1 / p" | dc -`
echo "pgrey              $xg $uxg (peripheral grey)" >> report.sienax

${FSLDIR}/bin/fslmaths T1_brain_mask_pve_0 -mas T1_segvent T1_pve_0_segvent -odt float
S=`${FSLDIR}/bin/fslstats T1_pve_0_segvent -m -v`
xa=`echo $S | awk '{print $1}'`
xb=`echo $S | awk '{print $3}'`
uxg=`echo "2 k $xa $xb * 1 / p" | dc -`
xg=`echo "2 k $xa $xb * $vscale * 1 / p" | dc -`
echo "vcsf               $xg $uxg (ventricular CSF)" >> report.sienax

S=`${FSLDIR}/bin/fslstats T1_brain_mask_pve_1 -m -v`
xa=`echo $S | awk '{print $1}'`
xb=`echo $S | awk '{print $3}'`
ugrey=`echo "2 k $xa $xb * 1 / p" | dc -`
ngrey=`echo "2 k $xa $xb * $vscale * 1 / p" | dc -`
echo "GREY               $ngrey $ugrey" >> report.sienax

S=`${FSLDIR}/bin/fslstats T1_brain_mask_pve_2 -m -v`
xa=`echo $S | awk '{print $1}'`
xb=`echo $S | awk '{print $3}'`
uwhite=`echo "2 k $xa $xb * 1 / p" | dc -`
nwhite=`echo "2 k $xa $xb * $vscale * 1 / p" | dc -`
echo "WHITE              $nwhite $uwhite" >> report.sienax

ubrain=`echo "2 k $uwhite $ugrey + 1 / p" | dc -`
nbrain=`echo "2 k $nwhite $ngrey + 1 / p" | dc -`
echo "BRAIN              $nbrain $ubrain" >> report.sienax


# bb_IDP_T1_SIENAX modified for lesion masked results
#Setting the string of NaN in case there is a problem.
numVars="11"
result="";
for i in $(seq 1 $numVars) ; do 
    result="NaN $result" ; 
done 
# Deriving only numbers from sienax report
if [ ! -f T1_sienax_lm.txt ] && [ -f report.sienax ] ; then
  echo `cat report.sienax` | cut -d " " -f2,7,8,12,13,17,18,20,21,23,24 > T1_sienax_lm.txt
  result=`cat T1_sienax_lm.txt` 

elif [ -f T1_sienax_lm.txt ] ; then
    result=`cat T1_sienax_lm.txt` 
fi
# copying in IDP folder
mkdir -p ${basedir}/IDP_files
echo $result > ${basedir}/IDP_files/bb_IDP_T1_SIENAX_lm.txt

cd $origDir