#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# Script name:  mc_dMRI_psmd_cortMDMO.sh
#
# Description:  Script to PSMD, cortical MD and MO
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------

. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header 

cd $1

#psmd calculation using MD skeleton from dMRI pipeline
mdskel=dMRI/TBSS/stats/all_MD_skeletonised.nii.gz

a=$(fslstats "${mdskel}" -P 95)
b=$(fslstats "${mdskel}" -P 5)
psmdresult=$(echo - | awk "{print ($a - $b )}" | sed 's/,/./')

if [ ! -d IDP_files ]; then
    mkdir IDP_files
fi
echo $psmdresult > IDP_files/bb_IDP_dMRI_psmd.txt

#cortical MD and MO for 5 atlas-based ROIs plus hipp and amygdala from FIRST
if [ ! -d dMRI/cortMD ]; then
    mkdir dMRI/cortMD
fi

#if the cortical ROIs are not already listed, put them in a directory
if [ ! -d $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois ]; then
    mkdir $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 66.5 -uthr 67.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/parahippL
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 67.5 -uthr 68.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/parahippR
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 66.5 -uthr 67.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/parahippL
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 66.5 -uthr 67.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/parahippL
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 60.5 -uthr 61.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/precuneousL
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 61.5 -uthr 62.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/precuneousR
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 4.5 -uthr 5.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supFCL
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 5.5 -uthr 6.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supFCR
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 34.5 -uthr 35.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supPCL
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 35.5 -uthr 36.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supPCR
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 36.5 -uthr 37.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargLant
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 37.5 -uthr 38.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargRant
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 38.5 -uthr 39.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargLpost
    ${FSLDIR}/bin/fslmaths $templ/GMatlas/GMatlas.nii.gz -thr 39.5 -uthr 40.5 -bin $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargRpost
    ${FSLDIR}/bin/fslmaths $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargLant -add $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargLpost $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargL
    ${FSLDIR}/bin/fslmaths $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargRant -add $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargRpost $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramargR
    rm $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramarg[LR]ant.nii.gz
    rm $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/supramarg[LR]post.nii.gz
fi

#check if T1-> dti transform is already available, if not make it 
flirt -in T1/T1_brain -ref fieldmap/fieldmap_iout  -applyxfm -usesqform -omat dMRI/T1_to_dti.mat -out dMRI/T1_to_dti

#transform gm pve map into dti space
flirt -in T1/T1_fast_sienax_lm/T1_brain_mask_pve_1 -ref fieldmap/fieldmap_iout -out dMRI/GM_inDiff -init dMRI/T1_to_dti.mat -applyxfm 

#Build warp for MNI -> dti
${FSLDIR}/bin/convertwarp --ref=fieldmap/fieldmap_iout --warp1=T1/transforms/T1_to_MNI_warp_coef_inv.nii.gz  --postmat=dMRI/T1_to_dti.mat --out=dMRI/MNI_to_dti_warp.nii.gz

#Transform ROIs std -> diffusion space 
for roi in `ls -1 $BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois`; do
    base=`remove_ext $roi`
    ${FSLDIR}/bin/applywarp --ref=dMRI/dMRI/dti_MD --in=$BB_BIN_DIR/bb_diffusion_pipeline/cortMDrois/$roi --out=dMRI/cortMD/${base}_inDiff --warp=dMRI/MNI_to_dti_warp 
done

#Transform csf-masked hipps into diffusion space and add to same directory
L_hipp=`ls T1/T1_first/T1_first_all_fast_firstseg_L_hipp_*_0_7.nii.gz`
R_hipp=`ls T1/T1_first/T1_first_all_fast_firstseg_R_hipp_*_0_7.nii.gz`
${FSLDIR}/bin/flirt -in $L_hipp -ref dMRI/dMRI/dti_FA -o dMRI/cortMD/L_hipp_inDiff -init dMRI/T1_to_dti.mat -applyxfm
${FSLDIR}/bin/flirt -in $R_hipp -ref dMRI/dMRI/dti_FA -o dMRI/cortMD/R_hipp_inDiff -init dMRI/T1_to_dti.mat -applyxfm

# Extract amygdalas from FIRST output then transform them in diffusion space too
${FSLDIR}/bin/fslmaths T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 17.5 -uthr 18.5 dMRI/cortMD/L_amyg_inT1
${FSLDIR}/bin/fslmaths T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 53.5 -uthr 54.5 dMRI/cortMD/R_amyg_inT1
${FSLDIR}/bin/flirt -in dMRI/cortMD/L_amyg_inT1 -ref dMRI/dMRI/dti_FA -o dMRI/cortMD/L_amyg_inDiff -init dMRI/T1_to_dti.mat -applyxfm
${FSLDIR}/bin/flirt -in dMRI/cortMD/R_amyg_inT1 -ref dMRI/dMRI/dti_FA -o dMRI/cortMD/R_amyg_inDiff -init dMRI/T1_to_dti.mat -applyxfm

#Calculate weighted MD and MO for each ROI
for roi in `ls -1 dMRI/cortMD/ | grep "inDiff"`; do 
    fslmaths dMRI/GM_inDiff -mul dMRI/cortMD/${roi} dMRI/cortMD/tmpWeighting
    fslmaths dMRI/dMRI/dti_MD -mul dMRI/cortMD/tmpWeighting dMRI/cortMD/tmpMD
    fslmaths dMRI/dMRI/dti_MO -mul dMRI/cortMD/tmpWeighting dMRI/cortMD/tmpMO
    roiMD=`fslstats dMRI/cortMD/tmpMD -M`
    roiMO=`fslstats dMRI/cortMD/tmpMO -M`
    mask=`fslstats dMRI/cortMD/tmpWeighting -M`
    weightedMD=`echo "scale=6; $roiMD / $mask" | bc`
    weightedMO=`echo "scale=6; $roiMO / $mask" | bc`
    resultMD="$resultMD $weightedMD"
    resultMO="$resultMO $weightedMO"
done

#Export IDPs to .txt files and export list with ROI names
echo $resultMD > IDP_files/cortMD.txt
echo $resultMD > dMRI/cortMD/cortMD.txt
echo $resultMO > IDP_files/cortMO.txt
echo $resultMO > dMRI/cortMD/cortMO.txt
echo `ls -1 dMRI/cortMD/ | grep "inDiff"` > dMRI/cortMD/cortMDrois.txt

cd ..