#!/bin/bash 
# ------------------------------------------------------------------------------
# Script name:  mc_T1_NBMvols.sh
#
# Description:  Script to calculate volumes of the left and right Nucleus Basalis of Meynert using a widely used histologically-defined NBM mask (Zaborszky et al., 2008). 
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------

baseT1=$1/T1

if [ -f $baseT1/transforms/T1_to_MNI_warp_coef_inv.nii.gz ] ; then
    #Left Ch4 
    ${FSLDIR}/bin/applywarp -i $BB_BIN_DIR/bhc_extras/dementia_idps/atlases/NBM/L_Ch4_mni.nii.gz -o $baseT1/LCh4_to_T1 -r $baseT1/T1 -w $baseT1/transforms/T1_to_MNI_warp_coef_inv.nii.gz --interp=nn
    Lresult=`${FSLDIR}/bin/fslstats -K $baseT1/LCh4_to_T1.nii.gz $baseT1/T1_fast_sienax_lm/T1_brain_mask_pve_1.nii.gz -m -v | xargs -n 3 | awk '{print "("$1"*"$2")"}' | bc `
    #Right Ch4 
    ${FSLDIR}/bin/applywarp -i $BB_BIN_DIR/bhc_extras/dementia_idps/atlases/NBM/R_Ch4_mni.nii.gz -o $baseT1/RCh4_to_T1 -r $baseT1/T1 -w $baseT1/transforms/T1_to_MNI_warp_coef_inv.nii.gz --interp=nn
    Rresult=`${FSLDIR}/bin/fslstats -K $baseT1/RCh4_to_T1.nii.gz $baseT1/T1_fast_sienax_lm/T1_brain_mask_pve_1.nii.gz -m -v | xargs -n 3 | awk '{print "("$1"*"$2")"}' | bc `
fi 

result=`echo $Lresult $Rresult`

# copying in IDP and T1 folders (creating it if does not exist)
mkdir -p $1/IDP_files
echo $result > $baseT1/NBM_vol_LR.txt
echo $result > $1/IDP_files/NBM_vol_LR.txt