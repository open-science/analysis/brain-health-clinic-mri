#!/bin/sh
# ------------------------------------------------------------------------------
# Script name:  mc_dMRI_tractvols.sh
#
# Description:  Script to calculate tract volumes
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------
# Calculate the tract volumes
for tract in `ls $1/dMRI/autoptx_preproc/tracts/*/tracts/tractsNorm.nii.gz`; do
    tractdir=`dirname $tract`
    #Transform tract from std space back into dMRI space
    applywarp -i $tract -r $1/dMRI/TBSS/FA/dti_FA -o ${tractdir}/tractsNorm_dMRI -w $1/dMRI/TBSS/FA/MNI_to_dti_FA_warp.nii.gz
    # Threshold and binarise tract then calculate volume
    fslmaths ${tractdir}/tractsNorm_dMRI -thr 0.005 -bin ${tractdir}/tractsNorm_dMRI_bin
    fslstats ${tractdir}/tractsNorm_dMRI_bin.nii.gz -V >> $1/dMRI/autoptx_preproc/tractvols.txt
done

# Extract tract volume IDPs
if [ -f $1/dMRI/autoptx_preproc/tractvols.txt ] ; then 
    # Set the name of the input and output files
    input_file=$1/dMRI/autoptx_preproc/tractvols.txt
    output_file=$1/IDP_files/AutoPtx_tract_volumes.txt

    # Loop through the lines of the input file, extract the second value and append it to the output line
    output_line=""
    while read -r line; do
        output_line="$output_line $(echo $line | awk '{print $2}')"
    done < "$input_file"

    # Save the output line to the output file
    echo $subj $output_line >> "$output_file"
fi