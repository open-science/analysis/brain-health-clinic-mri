#!/bin/bash
# ------------------------------------------------------------------------------
# Script name:  mc_T1_lobevols.sh
#
# Description:  Script to calculate GM volumes in the 4 lobes using MNI Structural Atlas probability maps 
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------
set -e
cd $1/T1

#warp lobe maps into T1 space and binarise
if [ ! -d lobes ]; then
    mkdir lobes 
fi 

for lobe in "front" "par" "temp" "occ"; do 
    ${FSLDIR}/bin/applywarp -i $BB_BIN_DIR/bhc_extras/dementia_idps/atlases/lobes/mni_prob_${lobe}.nii.gz -r T1_brain -o lobes/${lobe}_inT1 -w transforms/T1_to_MNI_warp_coef_inv.nii.gz
    ${FSLDIR}/bin/fslmaths lobes/${lobe}_inT1 -thr 5 -bin lobes/${lobe}_inT1_mask
done

#mask corrected gm pve by lobe masks
for lobe in "front" "par" "temp" "occ"; do 
    ${FSLDIR}/bin/fslmaths T1_fast_sienax_lm/T1_brain_mask_pve_1 -mas lobes/${lobe}_inT1_mask lobes/${lobe}_GMpve
done 

#calculate idps
for lobe in "front" "par" "temp" "occ"; do 
    S=`${FSLDIR}/bin/fslstats lobes/${lobe}_GMpve -M -V`
    mean=`echo $S | awk '{print $1}'`
    size=`echo $S | awk '{print $3}'`
    vol=$(echo "$mean * $size" | bc)
    result="$result $vol" 
done

# copying in IDP and T1 folders (creating it if does not exist)
echo $result > lobes/lobe_vols_fpto.txt
echo $result > ../IDP_files/lobe_vols_fpto.txt

cd ../..