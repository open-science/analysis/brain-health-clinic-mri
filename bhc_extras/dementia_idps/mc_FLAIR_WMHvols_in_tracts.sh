#!/bin/bash
# ------------------------------------------------------------------------------
# Script name:  mc_FLAIR_WMHvols_in_tracts.sh
#
# Description:  Script to calculate WMH in the WM tracts defined with the JHU Atlas
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------

#transform the JHU ROIs into T1 space (same space as WMHs)
${FSLDIR}/bin/applywarp -i $FSLDIR/data/atlases/JHU/JHU-ICBM-labels-1mm -o $1/T2_FLAIR/JHU_inT1 -r $1/T1/T1 -w $1/T1/transforms/T1_to_MNI_warp_coef_inv.nii.gz --interp=nn
result=`${FSLDIR}/bin/fslstats -K $1/T2_FLAIR/JHU_inT1.nii.gz $1/T2_FLAIR/lesions/final_mask.nii.gz -m -v | xargs -n 3 | awk '{print "("$1"*"$2")"}' | bc `

# copying in IDP and T1 folders (creating it if does not exist)
mkdir -p $1/IDP_files
echo $result > $1/T2_FLAIR/lesions/tractWMH_vols.txt
echo $result > $1/IDP_files/tractWMH_vols.txt