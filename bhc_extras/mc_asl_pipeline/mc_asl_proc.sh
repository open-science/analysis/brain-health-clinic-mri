#!/bin/bash

source $BB_BIN_DIR/bb_python/bb_python_asl_ukbb/bin/activate

OXFORD_ASL_DIR=$BB_BIN_DIR/bb_asl_pipeline/oxford_asl_ukb_dir
BBASL_ROI_DIR=$BB_BIN_DIR/bb_asl_pipeline/ukb_rois

cd $1/ASL

### Find TE
te=$(bb_read_json_field.py -F ASL_orig.json -f EchoTime -m 1000 -r 3)
                  
${OXFORD_ASL_DIR}/oxford_asl \
      -i ASL_merged \
      -o asl_output \
      -c calib \
      -s ../T1/T1.nii.gz \
      --regfrom-method=pwi --reg-init-bbr \
      --sbrain ../T1/T1_brain.nii.gz \
      --fastsrc ../T1/T1_fast_sienax_lm/T1_brain_mask \
      --warp=../T1/transforms/T1_to_MNI_warp \
      --fmap=../fieldmap/fieldmap_fout_to_T1_brain_rad \
      --fmapmag=../T1/T1 \
      --fmapmagbrain=../T1/T1_brain.nii.gz \
      --gdcwarp=ASL_ud_warp.nii.gz \
      --iaf=tc --ibf=rpt --mc=on --tis=1.65,1.9,2.15,2.4,2.65,2.9,3.15 --casl \
      --bolus 1.4 --fixbolus --spatial --tr 10 --te=$te \
      --echospacing=0.00022333333 \
      --slicedt 0.0445 \
      --pedir=-y \
      --gm-thresh=0.7 \
      --cmethod=voxel \
      --nofmapreg \
      --pvcorr \
      --region-analysis \
      --region-analysis-atlas=$BBASL_ROI_DIR/MNI_seg_max_prob_masked_RandL.nii.gz \
      --region-analysis-atlas-labels=$BBASL_ROI_DIR/MNI_seg_max_prob_masked_RandL.txt \
      --region-analysis-atlas=$BBASL_ROI_DIR/HO_L_Cerebral_WM_thr80.nii.gz \
      --region-analysis-atlas-labels=$BBASL_ROI_DIR/HO_L_Cerebral_WM_thr80.txt \
      --region-analysis-atlas=$BBASL_ROI_DIR/HO_R_Cerebral_WM_thr80.nii.gz \
      --region-analysis-atlas-labels=$BBASL_ROI_DIR/HO_R_Cerebral_WM_thr80.txt \
      --region-analysis-atlas=$BBASL_ROI_DIR/VascularTerritories_ero.nii.gz \
      --region-analysis-atlas-labels=$BBASL_ROI_DIR/VascularTerritories_ero.txt \
      --region-analysis-save-rois \
      --qc-output


