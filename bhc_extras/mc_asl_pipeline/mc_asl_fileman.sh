#!/bin/bash

if [[ -d $1/ASL ]] ; then 
    cd $1/ASL
    mkdir other
    for file in `ls *json` ; do
        echo $file
        base=`basename $file .json`
        series=`cat $file | grep "ImageType"`
        if [[ $series == *"ORIGINAL"*"NORM"* ]] ; then
            mv $file ASL_orig.json
            mv $base.nii.gz ASL_orig.nii.gz
            else
            mv $file other/$file
            mv $base.nii.gz other/$base.nii.gz
        fi
    done
    else
    echo "No ASL found"
fi