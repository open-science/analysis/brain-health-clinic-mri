#!/bin/env python

import os,sys,argparse
import bb_pipeline_tools.bb_logging_tool as LT
import bb_pipeline_tools.bb_file_manager as FM

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def mc_pipeline_asl(subject, jobHold):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    baseDir = logDir[0:logDir.rfind('/logs/')]

    jobASL_1 = LT.runCommand(logger, '${FSLSUBDIR}/fsl_sub -q short  -N "mc_asl_fileman_'   + subject + '" -j ' + str(jobHold)  + ' -l ' + logDir + ' $BB_BIN_DIR/bb_asl_pipeline/mc_asl_fileman.sh '   + subject )
    jobASL_2 = LT.runCommand(logger, '${FSLSUBDIR}/fsl_sub -q short  -N "mc_asl_preproc_'     + subject + '" -j ' + str(jobASL_1) + ' -l ' + logDir + ' $BB_BIN_DIR/bb_asl_pipeline/mc_asl_preproc.sh '     + subject )
    jobASL_3 = LT.runCommand(logger, '${FSLSUBDIR}/fsl_sub -q short  -N "mc_asl_proc_' + subject + '" -j ' + str(jobASL_2) + ' -l ' + logDir + ' $BB_BIN_DIR/bb_asl_pipeline/mc_asl_proc.sh ' + subject )
    jobASL_4 = LT.runCommand(logger, '${FSLSUBDIR}/fsl_sub -q short  -N "mc_asl_get_IDPs_' + subject + '" -j ' + str(jobASL_3) + ' -l ' + logDir + ' $BB_BIN_DIR/bb_asl_pipeline/mc_asl_get_IDPs ' + subject )
    
    return jobASL_4

def main(): 
  
    parser = MyParser(description='BioBank ASL Tool adapted for MC')
    parser.add_argument("subjectFolder", help='Subject Folder')

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]
    
    fileConfig = FM.bb_file_manager(subject)

    jobSTEP1 = mc_pipeline_asl(subject, '-1', fileConfig)
    
    print(jobSTEP1)
             
if __name__ == "__main__":
    main()
