#!/usr/bin/env bash
cd $1/ASL

#Get CALIB image
fslroi ASL_orig.nii.gz \
calib.nii.gz 0 -1 0 -1 0 -1 0 1

#Rename ASL image
fslroi ASL_orig.nii.gz ASL_merged.nii.gz 1 -1

#make everything readable 
chmod -R 775 *

#Gradient distortion correction applied
$BB_BIN_DIR/bb_pipeline_tools/bb_GDC_half_voxel \
--workingdir=ASL_GDC/ \
--in=calib.nii.gz \
--out=ASL_M0_ud.nii.gz \
--owarp=ASL_ud_warp.nii.gz \
--coeff=$BBDIR/bhc_data/OHBA_Prisma_coeff.grad