#!/bin/bash

ukb_pipeline=<PIPELINE_PATH> # USER TO MODIFY – path to the UKB pipeline, pointing at the folder that contains the activate_pipeline.sh script (absolute paths recommended).
subjdir=<SUBJ_DIR> # USER TO MODIFY – path to the main folder containing single-subject folders (absolute paths recommended).
coeff_file=<GDC_coefficient_file> # USER TO MODIFY – scanner-specific file containing information to perform gradient distortion correction (absolute paths recommended).
config_file=<ideal_config_sizes.json> # USER TO MODIFY – File containing expected image dimensions. Example from OBHC provided in run_ukb (absolute paths recommended).
naming_file=<naming_pattern.json> # USER TO MODIFY – File containing expected file names. Example from OBHC provided in run_ukb (absolute paths recommended).

source $ukb_pipeline/activate_pipeline.sh

subj=$1
if [ ! -e $subjdir/$subj/logs/file_descriptor.json ]; then    
    file_manager.sh $subj
fi

if [[ `cat $subjdir/$subj/logs/file_descriptor.json | wc -l` == 156 ]]; then 
        echo "All modalities present for $subj"
        if [[ `ls $subjdir/$subj/DICOM | wc -l` != 18 ]]; then
            dicom_extraction.sh $subj
        fi
        if [[ `ls $subjdir/$subj/DICOM | wc -l` == 18 ]]; then
            cd $subjdir
            bb_pipeline_queue.py -c $coeff_file -Q $config_file -P $naming_file $subj
        else
            echo "Check $subj DICOM extraction"
        fi
elif [[ `cat $subjdir/$subj/logs/file_descriptor.json | wc -l` == 146 ]]; then 
        echo "Clinical modalities present for $subj"
        if [[ `ls $subjdir/$subj/DICOM | wc -l` != 9 ]]; then
            dicom_extraction.sh $subj
        fi
        if [[ `ls $subjdir/$subj/DICOM | wc -l` == 9 ]]; then
            cd $subjdir
            bb_pipeline_queue.py -c $coeff_file -Q $config_file -P $naming_file $subj
        else
            echo "Check $subj DICOM extraction"
        fi
else
    echo "Check $subj files - do you want to continue anyway?"
    read varname 
    if [[ $varname == 'y' ]]; then 
        bhc_dicom_extraction.sh $subj
        cd $subjdir
        bb_pipeline_queue.py -c $coeff_file -Q $config_file -P $naming_file $subj
    fi
fi