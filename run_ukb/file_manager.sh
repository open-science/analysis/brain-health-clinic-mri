#!/bin/bash
#file_manager.sh

if [ "$1" == "" ];then
  echo "Usage:  `basename $0` <subject>"
  echo "Organises file names and structure and generates configuration file (file_descriptor.json)"
  echo "to be able to run UKB pipeline on BHC data"
  exit 0
fi

subject=$1

niftidir=PATH_TO_NIFTI #USER TO MODIFY
dicomdir=PATH_TO_DICOMS #USER TO MODIFY
inputdir=${niftidir}/${subject} 
outputdir=PATH_TO_OUTPUTDIR/${subject} #USER TO MODIFY
logdir=${outputdir}/logs

# Patterns specific for BHC scans acquired in OHBA. Should be fairly flexible to change here for other uses
t1pattern=*T1_p2*
t2pattern=*flair*
swipattern=*swi*
# details patterns for swi sub-files
phae1=*_echo1_*_ph.*
phae2=*_echo2_*_ph.*
mage1=*_echo1_??.*
mage2=*_echo2_??.*
swi_cH=*coilH[0-9]*
dmri3scan=*diff_3scan*
dmripattern=*diff*MB3*
rfmripattern=*resting*
aslpattern=*ASL*

if [ ! -d $outputdir ] ; then
    mkdir $outputdir
fi
cd $outputdir
mkdir logs raw DICOM

# If modality present, create directory, copy data, rename data and add to file_descriptor.json

############ T1 WEIGHTED #########
t1files=`ls $inputdir/$t1pattern 2> /dev/null | wc -l`
if [ $t1files == 0 ] ; then
    echo T1 not found cannot execute pipeline
    echo T1 not found cannot execute pipeline >> logs/bhc_manager_errors_log.txt
    else echo T1 found
    mkdir T1
    cp $inputdir/$t1pattern T1
    # start file descriptor if not already present, otherwise will only add new lines
    if [ ! -f logs/file_descriptor.json ] ; then
        echo "{" >> logs/file_descriptor.json
    fi

    cd T1
    # find norm and not norm file
    # T1 need to find normalised one from json
    #Not normalised has:
    #	"ImageType": ["ORIGINAL", "PRIMARY", "M", "ND"],
    #Normalised (to use) has:
    #	"ImageType": ["ORIGINAL", "PRIMARY", "M", "ND", "NORM"],
    for file in `ls *json` ; do
        echo $file
        base=`basename $file .json`
        series=`cat $file | grep "ImageType"`
        if [[ $series == *"NORM"* ]] ; then
            mv $file T1.json
            mv $base.nii.gz T1.nii.gz
            echo "    \"T1\": \"T1/T1.nii.gz\"," >> ../logs/file_descriptor.json
            else
            mv $file ../raw/T1_notNorm.json
            mv $base.nii.gz ../raw/T1_notNorm.nii.gz
            echo "    \"T1_notNorm\": \"raw/T1_notNorm.nii.gz\"," >> ../logs/file_descriptor.json
        fi
    done
    cd $outputdir
fi # if T1 found

######### T2-FLAIR WEIGHTED #########
t2files=`ls $inputdir/$t2pattern 2> /dev/null | wc -l`
if [ $t2files == 0 ] ; then
    echo T2_FLAIR not found
    echo T2_FLAIR not found >> logs/bhc_manager_errors_log.txt
    else echo T2_FLAIR found
    mkdir T2_FLAIR
    cp $inputdir/$t2pattern T2_FLAIR
    # start file descriptor if not already present, otherwise will only add new lines
    if [ ! -f logs/file_descriptor.json ] ; then
        echo "{" >> logs/file_descriptor.json
    fi

    cd T2_FLAIR
    # find norm and not norm file
    for file in `ls *json` ; do
        echo $file
        base=`basename $file .json`
        series=`cat $file | grep "ImageType"`
        if [[ $series == *"NORM"* ]] ; then
            mv $file T2_FLAIR.json
            mv $base.nii.gz T2_FLAIR.nii.gz
            echo "    \"T2\": \"T2_FLAIR/T2_FLAIR.nii.gz\"," >> ../logs/file_descriptor.json
            else
            mv $file ../raw/T2_FLAIR_notNorm.json
            mv $base.nii.gz ../raw/T2_FLAIR_notNorm.nii.gz
            echo "    \"T2_notNorm\": \"raw/T2_FLAIR_notNorm.nii.gz\"," >> ../logs/file_descriptor.json
        fi
    done
    cd $outputdir
fi #if T2_FLAIR found

######### SUSCEPTIBILITY WEIGHTED MRI #########
swifiles=`ls $inputdir/$swipattern 2> /dev/null | wc -l`
if [ $swifiles == 0 ] ; then
    echo SWI not found
    echo SWI not found >> logs/bhc_manager_errors_log.txt
    else echo SWI found
    mkdir SWI
    cp $inputdir/$swipattern SWI

    # start file descriptor if not already present, otherwise will only add new lines
    if [ ! -f logs/file_descriptor.json ] ; then
        echo "{" >> logs/file_descriptor.json
    fi

    cd SWI
    # remove "&" from filename as it breaks most tools
    for elem in * ; do 
        new_name=`echo $elem | sed 's|&|_and_|g'` ; 
        mv "$elem" "$new_name" ; 
    done

    # looking at separate channels first
    swiCHfiles=`ls $swi_cH 2> /dev/null | wc -l`
    if [ $swiCHfiles == 0 ] ; then
        echo SWI separate channels not found
        echo SWI separate channels not found - need to be created from scanner >> ../logs/bhc_manager_errors_log.txt
        else
        mkdir PHA_TE1 PHA_TE2 MAG_TE1 MAG_TE2
        mv *coilH[0-9]*_echo1_*_ph* PHA_TE1
        mv *coilH[0-9]*_echo2_*_ph* PHA_TE2
        mv *coilH[0-9]*_echo1_??.* MAG_TE1
        mv *coilH[0-9]*_echo2_??.* MAG_TE2
        # within each folder I force files to .nii.gz and write the relevant part of the file descriptor
        for folder in MAG_TE1 MAG_TE2 PHA_TE1 PHA_TE2 ; do
            echo "    \"SWI_$folder\": [" >> ../logs/file_descriptor.json
            cd $folder
            for file in `ls *.nii.gz` ; do
                base=`basename $file .nii.gz`
                echo "        \"SWI/$folder/$base.nii.gz\"," >> ../../logs/file_descriptor.json
            done
            cd ..
            # need to remove the final comma from the last file and then add ]
            lastline=`cat ../logs/file_descriptor.json | tail -1`
            echo $lastline
            newline=`echo $lastline | sed 's|,||g'`
            echo $newline
            sed '$d' ../logs/file_descriptor.json > ../logs/file_descriptor_new.json
            echo "        $newline" >> ../logs/file_descriptor_new.json
            echo "    ]," >> ../logs/file_descriptor_new.json
            mv ../logs/file_descriptor_new.json ../logs/file_descriptor.json
        done #for each folder with channels
    fi #if SWI channels exist

    # For the other swi files I need to read SeriesDescription from json
    # the first ones are generated for clinical use, so simply moving them away
    mkdir clinical clinical/SWI clinical/mIP
    for file in `ls *json` ; do
        echo $file
        base=`basename $file .json`
        series=`cat $file | grep "SeriesDescription"`
        if [[ $series == *"mIP_Images(SW)"* ]] ; then
            echo found clinical mIP
            mv $file clinical/mIP
            mv $base.nii.gz clinical/mIP
        fi
        if [[ $series == *"SWI_Images"* ]] ; then
            echo found clinical SWI
            mv $file clinical/SWI
            mv $base.nii.gz clinical/SWI
        fi
        if [[ $series == *"Mag_Images"* ]] ; then
            echo found clinical TOTAL MAG
            mv $file clinical
            mv $base.nii.gz clinical
        fi
        if [[ $series == *"Pha_Images"* ]] ; then
            echo found clinical TOTAL PHA
            mv $file clinical
            mv $base.nii.gz clinical
        fi
        # these are the ones that are needed by the pipeline
        if [[ $series == *"_P_RR"* ]] ; then
            echo OK SWI image to analyse with bb_pipeline
            # now need to rename files and add to file descriptor
            # cheched that total mag is normalised so we don't have SWI_TOTAL_MAG_notNorm files for swi (also in clical mag)
            if [[ $file == $phae1 ]] ; then
                mv $file SWI_TOTAL_PHA.json
                mv $base.nii.gz SWI_TOTAL_PHA.nii.gz
                echo "    \"SWI_TOTAL_PHA\": \"SWI/SWI_TOTAL_PHA.nii.gz\"," >> ../logs/file_descriptor.json
            fi
            if [[ $file == $phae2 ]] ; then
                mv $file SWI_TOTAL_PHA_TE2.json
                mv $base.nii.gz SWI_TOTAL_PHA_TE2.nii.gz
                echo "    \"SWI_TOTAL_PHA_TE2\": \"SWI/SWI_TOTAL_PHA_TE2.nii.gz\"," >> ../logs/file_descriptor.json
            fi
            if [[ $file == $mage1 ]] ; then
                mv $file SWI_TOTAL_MAG.json
                mv $base.nii.gz SWI_TOTAL_MAG.nii.gz
                echo "    \"SWI_TOTAL_MAG\": \"SWI/SWI_TOTAL_MAG.nii.gz\"," >> ../logs/file_descriptor.json
            fi
            if [[ $file == $mage2 ]] ; then
                mv $file SWI_TOTAL_MAG_TE2.json
                mv $base.nii.gz SWI_TOTAL_MAG_TE2.nii.gz
                echo "    \"SWI_TOTAL_MAG_TE2\": \"SWI/SWI_TOTAL_MAG_TE2.nii.gz\"," >> ../logs/file_descriptor.json
            fi
        fi #if file good to analyse
    done #all json in SWI
    cd $outputdir
fi #if SWI found

######### DIFFUSION WEIGHTED MRI #########
#first moving 3scan to clinical in case we want to analyse them at some point
dwi3files=`ls $inputdir/$dmri3scan 2> /dev/null | wc -l`
if [ $dwi3files == 0 ] ; then
    echo dMRI 3scan clinical not found
    echo dMRI 3scan clinical not found >> logs/bhc_manager_errors_log.txt
    else echo dMRI clinical found
    mkdir dMRI
    mkdir dMRI/clinical
    cp $inputdir/$dmri3scan dMRI/clinical
fi #if clinical dMRI found

dmrifiles=`ls $inputdir/$dmripattern 2> /dev/null | wc -l`
if [ $dmrifiles == 0 ] ; then
    echo dMRI not found
    echo dMRI not found >> logs/bhc_manager_errors_log.txt
    else echo dMRI found
    # directory should be there already as this is research and they will have done clinical first
    if [ ! -d dMRI ] ; then
        mkdir dMRI
    fi
    mkdir dMRI/raw
    cp $inputdir/$dmripattern dMRI/raw
    # start file descriptor if not already present, otherwise will only add new lines
    if [ ! -f logs/file_descriptor.json ] ; then
        echo "{" >> logs/file_descriptor.json
    fi

    cd dMRI/raw
    # # renaming bvals and bvec files first
    for encoding in AP PA ; do
        mv *${encoding}*bval ${encoding}.bval
        mv *${encoding}*bvec ${encoding}.bvec
        echo "    \"${encoding}_bval\": \"dMRI/raw/${encoding}.bval\"," >> ../../logs/file_descriptor.json
        echo "    \"${encoding}_bvec\": \"dMRI/raw/${encoding}.bvec\"," >> ../../logs/file_descriptor.json
    # renaming bvals and bvec files first
        for file in `ls *${encoding}*.json` ; do
            echo $file
            base=`basename $file .json`
            series=`cat $file | grep "ImageComments"`
            if [[ $series == *"Single-band reference"* ]] ; then
                echo found SBref ${encoding}
                mv $file ../../raw/${encoding}_SBREF.json
                mv $base.nii.gz ../../raw/${encoding}_SBREF.nii.gz
                echo "    \"${encoding}_SBRef\": \"raw/${encoding}_SBREF.nii.gz\"," >> ../../logs/file_descriptor.json
            else
                echo found dMRI ${encoding}
                mv $file ${encoding}.json
                mv $base.nii.gz ${encoding}.nii.gz
                echo "    \"${encoding}\": \"dMRI/raw/${encoding}.nii.gz\"," >> ../../logs/file_descriptor.json
            fi
        done
    done
    cd $outputdir
fi #if dMRI research found

######### RESTING STATE FMRI #########
rfmrifiles=`ls $inputdir/$rfmripattern 2> /dev/null | wc -l`
if [ $rfmrifiles == 0 ] ; then
    echo rfMRI not found
    echo rfMRI not found >> logs/bhc_manager_errors_log.txt
    else echo rfMRI found
    # directory should be there already as this is research and they will have done clinical first
    if [ ! -d fMRI ] ; then
        mkdir fMRI
    fi
    cp $inputdir/$rfmripattern fMRI
    # start file descriptor if not already present, otherwise will only add new lines
    if [ ! -f logs/file_descriptor.json ] ; then
        echo "{" >> logs/file_descriptor.json
    fi
    cd fMRI
    for file in `ls ${rfmripattern}.json` ; do
        echo $file
        base=`basename $file .json`
        series=`cat $file | grep "ImageComments"`
        if [[ $series == *"Single-band reference"* ]] ; then
            echo found SBref fMRI
            mv $file rfMRI_SBREF.json
            mv $base.nii.gz rfMRI_SBREF.nii.gz
            echo "    \"rfMRI_SBRef\": \"fMRI/rfMRI_SBREF.nii.gz\"," >> ../logs/file_descriptor.json
        else
            echo found fMRI
            mv $file rfMRI.json
            mv $base.nii.gz rfMRI.nii.gz
            echo "    \"rfMRI\": \"fMRI/rfMRI.nii.gz\"," >> ../logs/file_descriptor.json
        fi
    done
    cd $outputdir
fi #if rfMRI research found

######### ASL #########
aslfiles=`ls $inputdir/$aslpattern 2> /dev/null | wc -l`
if [ $aslfiles == 0 ] ; then
    echo ASL not found
    echo ASL not found >> logs/bhc_manager_errors_log.txt
    else echo ASL found
    mkdir ASL
    cp $inputdir/$aslpattern ASL
    # # start file descriptor if not already present, otherwise will only add new lines
    # if [ ! -f logs/file_descriptor.json ] ; then
    #     echo "{" >> logs/file_descriptor.json
    # fi
    cd ASL
    #for now I just copy the files and compress the nifti, not adding anything to file_descriptor
    for file in `ls *.nii.gz` ; do
        base=`basename $file nii.gz`
    done
    cd $outputdir
fi #if ASL

######### CLOSING CONFIGURATION FILE #########
# need to remove the final comma from the last file and then add }
lastline=`cat logs/file_descriptor.json | tail -1`
newline=`echo $lastline | sed 's|,||g'`
sed '$d' logs/file_descriptor.json > logs/file_descriptor_new.json
echo "    $newline" >> logs/file_descriptor_new.json
echo "}" >> logs/file_descriptor_new.json
mv logs/file_descriptor_new.json logs/file_descriptor.json
# display file_descriptor for a quick check
cat logs/file_descriptor.json 
##################