#!/bin/sh

#. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header

if [ "$1" == "" ];then
  echo "Usage:  `basename $0` <subject>"
  echo "Takes info from DICOMs - to be run after file_manager.sh"
  exit 0
fi

BB_BIN_DIR=PATH_TO_PIPELINE #USER TO MODIFY; ABSOLUTE FILEPATHS RECOMMENDED 
subjectID=$1

#USER TO MODIFY
dicomdir=PATH_TO_DICOM/${subjectID} #USER TO MODIFY
outputdir=PATH_TO_OUTPUTDIR/${subjectID}/DICOM #USER TO MODIFY
echo ${outputdir}
# Patterns specific for BHC scans acquired in OHBA. Should be fairly flexible to change here for other uses
t1pattern=*T1_p2*
t2pattern=*flair*
swipattern=*swi*
# for dMRI I look for the main one only, dMRI AP
dmripattern=*diff*AP*MB3*
rfmripattern=*resting*
aslpattern=*ASL*

cd $dicomdir
# removing spaces in directory names
for f in *\ *; do mv "$f" "${f// /_}"; done
# removing brackets in directory names
for elem in `ls -d *\(*\)*` ; do 
    new_name=`echo $elem | sed 's|[()]|_|g'` ;
    echo $new_name
    mv "$elem" "$new_name" ; 
done

t1dicomdir=`ls -d $t1pattern | sort | tail -n 1`
namT1=`ls    --color=none ${t1dicomdir}/* | sort | head -n 1`

t2dicomdir=`ls -d $t2pattern | sort | tail -n 1`
namT2=`ls    --color=none ${t2dicomdir}/* | sort | head -n 1`

swidicomdir=`ls -d $swipattern | sort | tail -n 1`
namSWI=`ls   --color=none ${swidicomdir}/* | sort | head -n 1`

dmridicomdir=`ls -d $dmripattern | sort | tail -n 1`
namdMRI=`ls  --color=none ${dmridicomdir}/* | sort | head -n 1`

rfmridicomdir=`ls -d $rfmripattern | sort | tail -n 1`
namrfMRI=`ls --color=none ${rfmridicomdir}/* | sort | head -n 1`

asldicomdir=`ls -d $aslpattern | sort | tail -n 1`
namASL=`ls   --color=none ${asldicomdir}/* | sort | head -n 1`

if [ ! "$namT1" == "" ] ; then
    cp $namT1 ${outputdir}/T1.dcm
    cat ${outputdir}/T1.dcm | strings > ${outputdir}/T1_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ${outputdir}/T1.dcm --all > ${outputdir}/T1.txt
fi

if [ ! "$namT2" == "" ] ; then
    cp $namT2 ${outputdir}/T2.dcm
    cat ${outputdir}/T2.dcm | strings > ${outputdir}/T2_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ${outputdir}/T2.dcm --all > ${outputdir}/T2.txt
fi

if [ ! "$namSWI" == "" ] ; then
    cp $namSWI ${outputdir}/SWI.dcm
    cat ${outputdir}/SWI.dcm | strings > ${outputdir}/SWI_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ${outputdir}/SWI.dcm --all > ${outputdir}/SWI.txt
fi

if [ ! "$namdMRI" == "" ] ; then
    cp $namdMRI ${outputdir}/dMRI.dcm
    cat ${outputdir}/dMRI.dcm | strings > ${outputdir}/dMRI_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ${outputdir}/dMRI.dcm --all > ${outputdir}/dMRI.txt
fi

if [ ! "$namrfMRI" == "" ] ; then
    cp $namrfMRI ${outputdir}/rfMRI.dcm
    cat ${outputdir}/rfMRI.dcm | strings > ${outputdir}/rfMRI_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ${outputdir}/rfMRI.dcm --all > ${outputdir}/rfMRI.txt
fi

if [ ! "$namASL" == "" ] ; then
    cp $namASL ${outputdir}/ASL.dcm
    cat ${outputdir}/ASL.dcm | strings > ${outputdir}/ASL_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ${outputdir}/ASL.dcm --all > ${outputdir}/ASL.txt
fi

echo ${outputdir}
