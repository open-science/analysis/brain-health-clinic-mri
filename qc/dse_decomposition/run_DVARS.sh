#!/bin/sh
# ------------------------------------------------------------------------------
# Script name:  run_DVARS.sh
#
# Description:  Example script to call DSE decomposition for rfMRI QC
# Dependency:   DSE Decomposition tool (https://github.com/asoroosh/DVARS/tree/master)
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------

if [ -e $1/fMRI ]; then
    fsl_sub -q short -s openmp,2 matlab_DVARS_call $1
fi