#!/bin/bash
# ------------------------------------------------------------------------------
# Script name:  eddy_quad.sh
#
# Description:  Script to run EDDY QUAD for dMRI QC
# Dependency:   EDDY QUAD tool (https://github.com/mabast85/eddy_qc_release)
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------

export PYTHONPATH=$BBDIR/eddy_qc/ #USER TO MODIFY; SET TO INSTALL LOCATION

eddy_quad $1/dMRI/dMRI/data -idx $1/dMRI/dMRI/eddy_index.txt -par $1/fieldmap/acqparams.txt -m $1/dMRI/dMRI/nodif_brain_mask.nii.gz -b $1/dMRI/dMRI/bvals -f $1/fieldmap/fieldmap_fout.nii.gz -g $1/dMRI/dMRI/bvecs -o $1/QC_files/quad