#!/bin/sh
# ------------------------------------------------------------------------------
# Script name:  mriqc.sh
#
# Description:  Script to create bids structure and submit MRIQC for T1, FLAIR, and rfMRI
# Dependency: MRIQC tool (https://github.com/nipreps/mriqc). See https://mriqc.readthedocs.io/ for usage documentation. 
#
# Author:       Grace Gillis, 2024
#
# ------------------------------------------------------------------------------

BB_BIN_DIR=PATH_TO_PIPELINE #USER TO MODIFY; ABSOLUTE FILEPATHS RECOMMENDED 
outputdir=PATH_TO_OUTPUTDIR #USER TO MODIFY

#UKB to bids conversion as prep for MRIQC 
oldname=$1
newname=${oldname/W3T_2019_102_/W3T2019102} #USER TO MODIFY ACCORDING TO ID SYNTAX
mkdir -p $1/QC_files/MRIQC/sub-${newname}
cd $1/QC_files/MRIQC/sub-${newname}

echo "{ 
    "\""BIDSVersion"\"": "\""1.0.0"\"",
    "\""Name"\"": "\""sub-W3T2019102200"\""
}" >> ../dataset_description.json #USER TO MODIFY ACCORDING TO ID SYNTAX

mkdir anat func dwi
cp ../../../T1/T1_orig.nii.gz anat/sub-${newname}_T1w.nii.gz
cp ../../../T1/T1.json anat/sub-${newname}_T1w.json
cp ../../../T2_FLAIR/T2_FLAIR_orig.nii.gz anat/sub-${newname}_T2w.nii.gz
cp ../../../T2_FLAIR/T2_FLAIR.json anat/sub-${newname}_T2w.json
if [ -e ../../../fMRI ]; then 
    cp ../../../fMRI/rfMRI.nii.gz func/sub-${newname}_task-rest_bold.nii.gz
    cp ../../../fMRI/rfMRI.json func/sub-${newname}_task-rest_bold.json
    sed -i '$s/}/,\n"TaskName":"rest"}/' func/sub-${newname}_task-rest_bold.json
    cp ../../../fMRI/rfMRI_SBREF.nii.gz func/sub-${newname}_task-rest_sbref.nii.gz
    cp ../../../fMRI/rfMRI_SBREF.json func/sub-${newname}_task-rest_sbref.json
    echo "singularity run -B /vols:/vols $BB_BIN_DIR/qc/mriqc_singularity/mriqc-0.15.1.simg ../ ../ participant --participant_label $newname -m bold --no-sub --n_procs 4 --mem_gb 8" >> ../mriqc_bold_cmd.txt
    fsl_sub -N "bb_rfmriqc" -l $outputdir/$1/logs/ -t ../mriqc_bold_cmd.txt
fi 

echo "singularity run -B /vols:/vols $BB_BIN_DIR/qc/mriqc_singularity/mriqc-0.15.1.simg ../ ../ participant --participant_label $newname -m T1w --no-sub --n_procs 4 --mem_gb 8" >> ../mriqc_cmds.txt
echo "singularity run -B /vols:/vols $BB_BIN_DIR/qc/mriqc_singularity/mriqc-0.15.1.simg ../ ../ participant --participant_label $newname -m T2w --no-sub --n_procs 4 --mem_gb 8" >> ../mriqc_cmds.txt

fsl_sub -N "bb_mriqc" -l $outputdir/$1/logs/ -t ../mriqc_cmds.txt
