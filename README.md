# BHC MRI analysis pipeline

Analysis pipeline for MRI data of the Oxford Brain Health Clinic (BHC), based on UK Biobank version 1.5

Code for the analysis pipeline presented in the papers:
> ["Adapting UK Biobank imaging for use in a routine memory clinic setting: the Oxford Brain Health Clinic"](https://www.sciencedirect.com/science/article/pii/S2213158222003382)

> ["From Big Data to the clinic: methodological and statistical enhancements to implement the UK Biobank imaging framework in a memory clinic" (preprint)](https://www.medrxiv.org/content/10.1101/2024.08.02.24311402v1)

## How to cite

Griffanti L, Gillis G, O’Donoghue MC, Blane J, Pretorius PM, Mitchell R, Aikin N,  Lindsay K, Campbell J, Semple J, Alfaro-Almagro F, Smith SM, Miller KL, Martos L, Raymont V, Mackay CE; 2022; Adapting UK Biobank imaging for use in a routine memory clinic setting: the Oxford Brain Health Clinic; Neuroimage:Clinical; 36: 103273. https://doi.org/10.1016/j.nicl.2022.103273. 

Full paper openly available [here](https://www.sciencedirect.com/science/article/pii/S2213158222003382)


Gillis G, Bhalerao G, Blane J, Mitchell R, Pretorius PM, McCracken C, Nichols TE, Smith SM, Miller KL, Alfaro-Almagro F, Raymont V, Martos L, Mackay CE, Griffanti L; 2024; From Big Data to the clinic: Methodological and statistical enhancements to implement the UK Biobank imaging framework in a memory clinic.  medRxiv. https://doi.org/10.1101/2024.08.02.24311402

Preprint openly available [here](https://www.medrxiv.org/content/10.1101/2024.08.02.24311402v1)

## How to run
### 1. Run the main UK Biobank image processing pipeline (v1.5)
The [UKB pipeline (v1.5)](https://git.fmrib.ox.ac.uk/falmagro/uk_biobank_pipeline_v_1.5.git) is openly available. Example calls of the main UKB pipeline are available in the directory `run_ukb`, including the script `submit_pipeline.sh` which runs the preparatory steps, if necessary, then submits the main pipeline queue. 

Our only adaptation to the main UKB pipeline is the inclusion of the `-cnr` flag when running EDDY (uk_biobank_pipeline_v_1.5/bb_diffusion_pipeline/bb_eddy/bb_eddy_wrap_gpu). This does not change the performance of EDDY, but it saves the CNR maps which are required for the subsequent diffusion QC with EDDY QUAD (see below). 

### 2. Run the QC tools
- [MRIQC](https://mriqc.readthedocs.io/en/latest/): We used singularity container version 0.15.1. This container is available for download [here](https://git.fmrib.ox.ac.uk/mcz502/qc-paper/-/tree/main/MRIQC_singularity?ref_type=heads), or it can be built with the following command (further instructions available [here](https://andysbrainbook.readthedocs.io/en/latest/OpenScience/OS/MRIQC.html)): 
`singularity build $HOME/mriqc-0.15.1.simg docker://poldracklab/mriqc:0.15.1`
 MRIQC can be run in parallel with the main UKB pipeline or after it completes. As an example, the script we used to convert the T1, FLAIR, and rfMRI data to BIDS format and run MRIQC is available in the `qc` directory.
- EDDY QUAD: GitHub [repository](https://github.com/mabast85/eddy_qc_release) includes installation instructions. We used version 1.0.2. Because EDDY QUAD requires the outputs of EDDY, it is recommended to run it after the main UKB pipeline. The script we used to run eddy_quad is available as an example in the `qc` directory. 
- DSE decomposition: MATLAB and Python versions are available in the GitHub [repository](https://github.com/asoroosh/DVARS). We used the MATLAB verison, and the scripts we used to run DSE decomposition are available as examples in `qc/dse_decomposition`. Because this tool is run on the raw and preprocessed rfMRI data, we recommend running it after the main UKB pipeline. 

### 3. Run the adaptions and additions
Adaptations and additional analyses have been packaged into an additional UKB-style queue, `bhc_extras_queue.py`. Please note that ASL is included here because our parameters are different (ASL was included in UK Biobank after the start of OBHC), so our scripts differ accordingly. 

#### UK Biobank pipeline adaptation scripts
- `bhc_fast_and_sienax_with_lm.sh` Script to run a reduced version of SIENAX adapted with lesion-masking, as described in [Griffanti et al, 2022](https://doi.org/10.1016/j.nicl.2022.103273)
- `bhc_hipp_masking.sh` Script to mask out the CSF (pve threshold 0.7) from the FIRST hippocampal masks, as described in [Griffanti et al, 2022](https://doi.org/10.1016/j.nicl.2022.103273)

#### Downstream adaptation scripts
- `mc_asl_pipeline` Set of scripts to run ASL pipeline using the lesion-masked GM PVE map
- `mc_vbm_lm` Script to re-run the VBM analysis using the lesion-masked GM PVE
- `mc_swi_reg_clinical` Adapted version of swi_reg script in the UKB pipeline; runs SWI pipeline without requiring separate channel files. Optional depending on your data. 

#### Dementia-informed analysis scripts
- `mc_T1_lobevols.sh` Script to calculate GM volumes in the 4 lobes using MNI Structural Atlas probability maps 
- `mc_T1_NBMvols.sh` Script to calculate volumes of the left and right Nucleus Basalis of Meynert
- `mc_FLAIR_WMHvols_in_tracts.sh` Script to calculate WMH in the WM tracts defined with the JHU Atlas
- `mc_dMRI_psmd_cortMDMO.sh` Script to calculate PSMD, cortical MD and MO
- `mc_dMRI_tractvols.sh` Script to calculate tract volumes

## Dependencies
Official version of the UKB pipeline: https://git.fmrib.ox.ac.uk/falmagro/uk_biobank_pipeline_v_1.5/-/tree/master  

### QC Tools
- MRIQC: https://github.com/nipreps/mriqc
- EDDY QUAD: https://github.com/mabast85/eddy_qc_release 
- DSE Decomposition Tool: https://github.com/asoroosh/DVARS 

## Other resources
Oxford Brain Health Clinic
- BHC clinical protocol and research database: [O'Donoghue et al., 2023](https://bmjopen.bmj.com/content/13/8/e067808)
- BHC MRI acquisition protocol details: [O'Donoghue et al., 2022 zenodo](https://zenodo.org/record/6598036#.Ys1rCC9r2Ak)
- [Oxford Brain Health Clinic website](https://oxfordhealthbrc.nihr.ac.uk/our-work/brain-health-centre/)

UK Biobank brain imaging
- [UK Biobank Brain Imaging Documentation](https://biobank.ndph.ox.ac.uk/showcase/showcase/docs/brain_mri.pdf)
- [UK Biobank Brain Imaging - Online Resources](https://www.fmrib.ox.ac.uk/ukbiobank/)
