#!/bin/env python

'''
 Authors: Grace Gillis
 01-11-2024
 Version $1.0
'''

import os,sys,argparse
import bb_pipeline_tools.bb_logging_tool as LT
import bb_pipeline_tools.bb_file_manager as FM
from bhc_extras.mc_asl_pipeline.mc_pipeline_asl                    import mc_pipeline_asl

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def bb_pipeline_extras(subject, jobHold):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    baseDir = logDir[0:logDir.rfind('/logs/')]

    jobExtra_1 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_sienax_lm_'   + subject + '" -j ' + str(jobHold)  + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/bhc_fast_and_sienax_with_lm.sh '   + subject )
    jobExtra_2 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_hipp_masking_'     + subject + '" -j ' + str(jobHold) + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/bhc_hipp_masking.sh '     + subject )
    jobExtra_3 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_vbm_lm_' + subject + '" -j ' + str(jobExtra_1) + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/mc_vbm_lm ' + subject )
    jobExtra_4 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_T1_lobevols_' + subject + '" -j ' + str(jobExtra_1) + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/dementia_idps/mc_T1_lobevols.sh ' + subject )
    jobExtra_5 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_T1_NBMvols_' + subject + '" -j ' + str(jobHold) + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/dementia_idps/mc_T1_NBMvols.sh ' + subject )
    jobExtra_6 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_FLAIR_WMHvols_' + subject + '" -j ' + str(jobExtra_1) + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/dementia_idps/mc_FLAIR_WMHvols_in_tracts.sh ' + subject )
    jobExtra_7 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_psmd_cortMDMO_' + subject + '" -j ' + str(jobHold) + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/dementia_idps/mc_dMRI_psmd_cortMDMO.sh ' + subject )
    jobExtra_8 = LT.runCommand(logger, 'fsl_sub -q short -N "mc_dMRI_tractvols_' + subject + '" -j ' + str(jobHold) + ' -l ' + logDir + ' $BB_BIN_DIR/bhc_extras/dementia_idps/mc_dMRI_tractvols.sh ' + subject )
    jobExtra_9 = mc_pipeline_asl(subject, jobExtra_1)   

    return jobExtra_9

def main(): 
  
    parser = MyParser(description='BioBank-Style Tool adapted for Running MC Extras')
    parser.add_argument("subjectFolder", help='Subject Folder')

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]
    
    #fileConfig = FM.bb_file_manager(subject)

    jobSTEP1 = bb_pipeline_extras(subject, '-1')
    
    return(jobSTEP1)
    LT.finishLogging(logger)

    #print(jobSTEP1)
             
if __name__ == "__main__":
    main()

